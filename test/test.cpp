#include "EarClipping.hpp"

#include <SFML/Graphics.hpp>

int main() {
  sf::RenderWindow window;
  window.create(sf::VideoMode(800,600), "Ear Clipping Demo", sf::Style::None);
  window.setFramerateLimit(60);

  std::vector<EarClipping::Point> points;
  std::vector<EarClipping::Triangle> triangles;
  while (window.isOpen()) {
    sf::Event event;
    while (window.pollEvent(event)) {
      switch (event.type) {
        case sf::Event::Closed:
          window.close();
          break;
        case sf::Event::KeyReleased:
          if (event.key.code == sf::Keyboard::Escape) {
            triangles.clear();
            points.clear();
          }
          break;
        case sf::Event::MouseButtonPressed:
          if (event.mouseButton.button == sf::Mouse::Left) {
            points.push_back({(float)event.mouseButton.x, (float)event.mouseButton.y});
          }
          else if (event.mouseButton.button == sf::Mouse::Right) {
            if (points.size() > 0)
              points.pop_back();
          }
          triangles = EarClipping::run(points);
        default:
          break;
      }
    }
    
    window.clear();

    sf::VertexArray shape(sf::LineStrip);
    for (auto const& p: points) {
      sf::CircleShape c;
      c.setRadius(5);
      c.setOrigin(5,5);
      c.setFillColor(sf::Color::Transparent);
      c.setOutlineColor(sf::Color::Red);
      c.setOutlineThickness(2);
      c.setPosition(p.x, p.y);
      window.draw(c);
      shape.append(sf::Vertex(sf::Vector2f(p.x, p.y), sf::Color::Green));
    }
    if (points.size() > 0)
      shape.append(shape[0]);

    sf::VertexArray tarray(sf::Triangles);
    for (auto const& t: triangles) {
      tarray.append(sf::Vertex(sf::Vector2f(t[0].x, t[0].y), sf::Color::Blue));
      tarray.append(sf::Vertex(sf::Vector2f(t[1].x, t[1].y), sf::Color::Green));
      tarray.append(sf::Vertex(sf::Vector2f(t[2].x, t[2].y), sf::Color::Yellow));
    }

    window.draw(tarray);
    window.draw(shape);

    window.display();
  }

  return 0;
}

#ifndef DEF_EARCLIPPING_HPP
#define DEF_EARCLIPPING_HPP

#include <array>
#include <numeric>
#include <vector>

class EarClipping {
  public:
    struct Point {
      float x, y;

      Point operator-(Point const& other) const {
        return {x - other.x, y - other.y};
      };
      
      Point operator+(Point const& other) const {
        return {x + other.x, y + other.y};
      };

      Point operator*(float val) const {
        return {x*val, y*val};
      };

      Point operator/(float val) const {
        return {x/val, y/val};
      };
    };
    typedef std::array<Point,3> Triangle;

  private:
    static float det(Point const& u, Point const& v) {
      return u.x*v.y - v.x*u.y;
    };

    static bool isInTriangle(Point const& p, Triangle const& t) {
      return 
        det(t[1]-t[0], p-t[0]) > 0 &&
        det(t[2]-t[1], p-t[1]) > 0 &&
        det(t[0]-t[2], p-t[2]) > 0;
    };

  public:
    static std::vector<Triangle> run(std::vector<Point> const& points) {
      std::vector<Triangle> ret;
      std::vector<size_t> index; // indexes of the points left in the polygon
      index.resize(points.size());
      std::iota(index.begin(), index.end(), 0);

      if (points.size() < 3)
        return {};

      bool triangleFound = true;
      // loop through all the points
      while (index.size() != 0) {
        if (!triangleFound) // if we don't find a triangle each loop, the polygon isn't triangulable
          return ret;
        triangleFound = false;
        
        // for each 3 vertices in the triangle
        for (auto it = index.begin(); it != index.end()-2; ++it) {
          size_t i0 = *it;
          size_t i1 = *(it+1);
          size_t i2 = *(it+2);
          // check if the triangle winds in the right direction
          if (det(points[i1]-points[i0], points[i2]-points[i1]) <= 0)
            continue;

          // good candidate, check if a point is inside it
          Triangle t{points[i0], points[i1], points[i2]};
          bool result = true;
          for (size_t i = 0; i < points.size(); i++) {
            if (i == i0 || i == i1 || i == i2)
              continue;
            if (isInTriangle(points[i], t)) {
              result = false;
              break;
            }
          }
          // candidate has a point inside it, skip it
          if (!result)
            continue;

          // we have an ear, remove it from the index vector and add it to return vector
          index.erase(it+1);
          ret.push_back(t);
          triangleFound = true;
          break; // stop searching ears for this loop
        }
      }
      return ret;
    };
};

#endif
